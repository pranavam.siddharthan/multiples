package org.xigma.multiples;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MultiplesTest {

	@Test
	public void givenASeries_whenRequestedForMultiplesPari_thenProvideThePair() {
		Map<Integer, Integer> findMultiples = new Multiples(10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120)
				.findMultiples(1200);

		Map<Integer, Integer> expectedMap = new HashMap<>();
		expectedMap.put(10, 120);
		expectedMap.put(20, 60);
		expectedMap.put(60, 20);
		expectedMap.put(30, 40);
		expectedMap.put(40, 30);
		expectedMap.put(120, 10);

		assertEquals(expectedMap, findMultiples);
	}

	@Test
	public void givenALargeSeries_whenRequestedForMultiplesPari_thenProvideThePair() {
		Map<Integer, Integer> findMultiples = new Multiples(-1_00_000, 1_00_000).findMultiples(1200);
		assertEquals(getExpectedMap(), findMultiples);
	}

	@Test
	public void givenALargeSeriesWithZero_whenRequestedForMultiplesPari_thenProvideThePair() {
		Map<Integer, Integer> findMultiples = new Multiples(-1_00_000, 1_00_000).findMultiples(1200);
		assertEquals(getExpectedMap(), findMultiples);
	}

	private Map<Integer, Integer> getExpectedMap() {
		Map<Integer, Integer> expectedMap = new HashMap<>();
		expectedMap.put(-1, -1200);
		expectedMap.put(1, 1200);
		expectedMap.put(-2, -600);
		expectedMap.put(2, 600);
		expectedMap.put(-3, -400);
		expectedMap.put(3, 400);
		expectedMap.put(-4, -300);
		expectedMap.put(4, 300);
		expectedMap.put(-5, -240);
		expectedMap.put(5, 240);
		expectedMap.put(-6, -200);
		expectedMap.put(6, 200);
		expectedMap.put(-8, -150);
		expectedMap.put(8, 150);
		expectedMap.put(-10, -120);
		expectedMap.put(10, 120);
		expectedMap.put(-12, -100);
		expectedMap.put(12, 100);
		expectedMap.put(-15, -80);
		expectedMap.put(15, 80);
		expectedMap.put(-400, -3);
		expectedMap.put(-16, -75);
		expectedMap.put(16, 75);
		expectedMap.put(400, 3);
		expectedMap.put(-20, -60);
		expectedMap.put(20, 60);
		expectedMap.put(-150, -8);
		expectedMap.put(150, 8);
		expectedMap.put(-24, -50);
		expectedMap.put(24, 50);
		expectedMap.put(-25, -48);
		expectedMap.put(25, 48);
		expectedMap.put(-30, -40);
		expectedMap.put(30, 40);
		expectedMap.put(-40, -30);
		expectedMap.put(40, 30);
		expectedMap.put(-300, -4);
		expectedMap.put(300, 4);
		expectedMap.put(-1200, -1);
		expectedMap.put(-48, -25);
		expectedMap.put(48, 25);
		expectedMap.put(1200, 1);
		expectedMap.put(-50, -24);
		expectedMap.put(50, 24);
		expectedMap.put(-60, -20);
		expectedMap.put(60, 20);
		expectedMap.put(-200, -6);
		expectedMap.put(200, 6);
		expectedMap.put(-75, -16);
		expectedMap.put(75, 16);
		expectedMap.put(-80, -15);
		expectedMap.put(80, 15);
		expectedMap.put(-600, -2);
		expectedMap.put(600, 2);
		expectedMap.put(-100, -12);
		expectedMap.put(100, 12);
		expectedMap.put(-240, -5);
		expectedMap.put(240, 5);
		expectedMap.put(-120, -10);
		expectedMap.put(120, 10);
		return expectedMap;
	}
}
