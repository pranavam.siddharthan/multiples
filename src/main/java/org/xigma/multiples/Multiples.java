package org.xigma.multiples;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Multiples {

	private int[] values;

	public Multiples(int... values) {
		this.values = values;
	}

	public Multiples(Integer start, Integer end) {
		this.values = IntStream.range(start, end).toArray();
	}

	public Map<Integer, Integer> findMultiples(int result) {
		List<Integer> valueList = arraysToList();

		return valueList.parallelStream()
				.filter(x -> x != 0)
				.filter(x -> valueList.contains(result / x))
				.filter(x -> (result / x) * x == result)
				.collect(Collectors.toMap(Function.identity(), x -> result / x));
	}

	private List<Integer> arraysToList() {
		List<Integer> list = new ArrayList<Integer>(values.length);
		for (Integer i : values) {
			list.add(i);
		}
		return list;
	}

}
